# extensions.gnome.org devcontainer workspace
## Quick start

1. Clone devcontainer project to some folder. Let's use `$HOME/extensions.gnome.org`
   ```bash
   git clone https://gitlab.gnome.org/nE0sIghT/ego-devcontainer.git extensions.gnome.org
   cd extensions.gnome.org
   ```
2. Clone frontend, backend and metadata projects
   ```bash
   git clone https://gitlab.gnome.org/nE0sIghT/extensions-frontend.git
   git clone https://gitlab.gnome.org/Infrastructure/extensions-web.git
   git clone https://gitlab.gnome.org/nE0sIghT/extensions-metadata-service.git
   ```
3. Install [Visual Studio Code](https://code.visualstudio.com/) with [Remote Development pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
4. Open ego.code-workspace using Visual Studio Code
5. Click `Reopen in container` when prompted.

## Available services

Within running devcontainer there are development services are available:

1. phpMyAdmin  
   http://localhost:8088  

   Login: user  
   Password: P@ssw0rd 

2. OpenSearch Dashboards  
   http://localhost:8090

## Next steps

At this point you have ready to go development environment.  
If you don't familiar with Django start with [Writing your first Django app](https://docs.djangoproject.com/en/3.2/intro/tutorial01/) tutorial.  
Probably you want to populate database using [`./manage.py migrate`](https://docs.djangoproject.com/en/3.2/ref/django-admin/#django-admin-migrate) and start application using `Run and Debug` VScode menu (F5).  
Before first run you need to initialize search backend using `./manage.py init_search` command.  
